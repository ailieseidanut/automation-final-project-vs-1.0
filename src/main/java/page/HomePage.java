package page;


import lombok.Getter;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;


@Getter
public class HomePage {
    WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    private String URL = "http://automationpractice.com/index.php";
    private String title = "My Store";

    @FindBy(id = "Women")
    private WebElement woman;

    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[2]/a")
    private WebElement dress;

    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[3]/a")
    private WebElement tShort;

    @FindBy(xpath = "//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a")
    private WebElement cart;

    @FindBy(xpath = "//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a")
    private WebElement login;

    @FindBy(xpath = "//*[@id=\"contact-link\"]/a")
    private WebElement contactUs;

    @FindBy(xpath = "//*[@id=\"homeslider\"]/li[4]/a")
    private WebElement homeSliderContain;

    @FindBy(id = "search_query_top")
    private WebElement searchField;

    @FindBy(xpath = "//*[@id=\"home-page-tabs\"]/li[1]/a")
    private WebElement popular;

    @FindBy(id = "email_create")
    public WebElement emailFill;

    @FindBy(id = "SubmitCreate")
    public WebElement submitCreate;

    @FindBy(id = "id_gender1")
    public WebElement titleGender;

    @FindBy(id = "customer_firstname")
    private WebElement firstName;

    @FindBy(id = "customer_lastname")
    private WebElement lastName;

    @FindBy(id = "passwd")
    public WebElement password;

    @FindBy(id = "days")
    public WebElement dayOfBirth;

    @FindBy(id = "months")
    public WebElement monthOfBirth;

    @FindBy(id = "years")
    public WebElement yearOfBirth;

    @FindBy(xpath = "//*[@id=\"account-creation_form\"]/div[1]/div[7]/label")
    public WebElement selectSignUpForOurNewsletter;

    @FindBy(id = "optin")
    public WebElement selectSignUpToReceive;

    @FindBy(id = "firstname")
    public WebElement firstNameYourAddress;

    @FindBy(id = "lastname")
    public WebElement lastNameYourAddress;

    @FindBy(id = "company")
    public WebElement company;

    @FindBy(id = "address1")
    public WebElement address;

    @FindBy(id = "address2")
    public WebElement addressLine2;

    @FindBy(id = "city")
    public WebElement city;

    @FindBy(id = "id_state")
    public WebElement state;

    @FindBy(id = "postcode")
    public WebElement postalCode;

    @FindBy(id = "id_country")
    public WebElement country;

    @FindBy(id = "other")
    public WebElement additionalInformation;

    @FindBy(id = "phone")
    public WebElement homePhone;

    @FindBy(id = "phone_mobile")
    public WebElement mobilePhone;

    @FindBy(id = "alias")
    public WebElement alias;

    @FindBy(id = "submitAccount")
    public WebElement registerButton;

    @FindBy(id = "email")
    public WebElement email;

    @FindBy(id = "SubmitLogin")
    public WebElement signInButton;

    @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li/div/div[2]/div[2]/a[1]/span")
    public WebElement addToCart;

    @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li/div")
    public WebElement addToCart2;

    @FindBy(xpath = "//*[@id=\"add_to_cart\"]/button")
    public WebElement addToCart3;

    @FindBy(xpath = "//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span")
    public WebElement continueShoping;

    @FindBy(xpath = "//*[@id=\"categories_block_left\"]/div/ul/li[1]/a")
    public WebElement cassualDress;

    @FindBy(css = "a[title*='Proceed to checkout']")
    private WebElement proceedToCheckoutButton;

    @FindBy(xpath = "//*[@id=\"center_column\"]/p[2]/a[1]/span")
    public WebElement proceedToCheckoutButtonSummary;

    @FindBy(xpath = "//*[@id=\"center_column\"]/form/p/button/span")
    public WebElement proceedToCheckoutButtonAddress;

    @FindBy(xpath = "//*[@id=\"form\"]/div/p[2]/label")
    public WebElement agreeWithTerms;

    @FindBy(xpath = "//*[@id=\"form\"]/p/button/span")
    public WebElement proceedToCheckoutButtonShipping;

    @FindBy(xpath = "//*[@id=\"HOOK_PAYMENT\"]/div[1]/div/p/a")
    public WebElement payByBankWire;

    @FindBy(xpath = "//*[@id=\"cart_navigation\"]/button/span")
    public WebElement confirmOrder;


    public void signInButton() {
        getLogin().click();
    }

    public void fillUserMail() {
        getEmailFill().sendKeys("ion127@gmail.com");
    }

    public void clickOnRegisterButton() {
        getSubmitCreate().click();
    }

    public void clickOnTitle() {
        getTitleGender().click();
    }

    public void fillFirstName() {
        getFirstName().sendKeys("Ion");
    }

    public void fillLastName() {
        getLastName().sendKeys("Popescu");
    }

    public void password() {
        getPassword().sendKeys("Abc123..");
    }

    public void select15DayOfBirth() {
        new Select(dayOfBirth).selectByIndex(15);
    }

    public void selectJulyMonthOfBirth() {
        new Select(monthOfBirth).selectByIndex(7);
    }

    public void select1980YearOfBirth() {
        new Select(yearOfBirth).selectByIndex(42);
    }

    public void clickSelectSignUpForOurNewsletter() {
        getSelectSignUpForOurNewsletter().click();
    }

    public void clickOnReceiveOffers() {
        getSelectSignUpToReceive().click();
    }

    public void fillFirstNameInYourAddress() {
        getFirstName().clear();
        getFirstName().sendKeys("Crist");
    }

    public void fillLastNameInYourAddress() {
        getLastName().clear();
        getLastName().sendKeys("Georgescu");
    }

    public void fillcompanyName() {
        getCompany().sendKeys("Ion's Company");
    }

    public void fillAddress() {
        getAddress().sendKeys("Iasi, street Nationala 56");
    }

    public void fillAddress1() {
        getAddressLine2().sendKeys("Iasi, street Nicolina 123");
    }

    public void fillCity() {
        getCity().sendKeys("Iasi");
    }

    public void selectState() {
        new Select(state).selectByIndex(11);
    }

    public void fillPostalCode() {
        getPostalCode().sendKeys("65788");
    }

    public void selectCountry() {
        new Select(country).selectByIndex(1);
    }

    public void additionalInformation() {
        getAdditionalInformation().sendKeys("hello world");
    }

    public void homePhone() {
        getHomePhone().sendKeys("0789653546");
    }

    public void mobilePhone() {
        getMobilePhone().sendKeys("0789653123");
    }

    public void assignAnAliasAddress() {
        getAlias().clear();
        getAlias().sendKeys("Bucharest");
    }

    public void registerButton() {
        getRegisterButton().click();
    }

    public void checkAccountCreation() {
        String bodyId = driver.findElement(By.tagName("body")).getAttribute("id");
        Assert.assertEquals("my-account", bodyId);
    }

    public void fillEmail() {
        getEmail().sendKeys("ion126@gmail.com");
    }

    public void userPassword() {
        getPassword().sendKeys("Abc123..");
    }

    public void signInButtonFromAlreadyRegister() {
        getSignInButton().click();
    }

    public void selectTShort() {
        getTShort().click();
    }

    public void addTocCartTShort() {
        Actions action = new Actions(driver);
        WebElement we = driver.findElement(By.xpath("//*[@id=\"center_column\"]/ul/li/div/div[1]/div/a[1]/img"));
        action.moveToElement(we).build().perform();
        getAddToCart().click();
    }

    public void continueShoping() {
        getContinueShoping().click();
    }

    public void clickOnDressButton() {
        getDress().click();
    }

    public void clickOnCassualDress() {
        getCassualDress().click();
    }

    public void addTocCartTShort2() {
        Actions action = new Actions(driver);
        WebElement we = driver.findElement(By.xpath("//*[@id=\"center_column\"]/ul/li[1]"));
        action.moveToElement(we).build().perform();
        getAddToCart2().click();
    }

    public void clickAddToCart3() {
        getAddToCart3().click();
    }

    public void proceededToCheckout() {
        getProceedToCheckoutButton().click();
    }

    public void clickproceedToCheckoutButtonSummary() {
        getProceedToCheckoutButtonSummary().click();
    }

    public void clickproceedToCheckoutButtonAddress() {
        getProceedToCheckoutButtonAddress().click();
    }

    public void agreewithTerms() {
        getAgreeWithTerms().click();
    }

    public void clickproceedToCheckoutButtonShipping() {
        getProceedToCheckoutButtonShipping().click();
    }

    public void clickPayByBankWire() {
        getPayByBankWire().click();
    }

    public void clickConfirmOrder() {
        getConfirmOrder().click();
    }

    public void confirmationPage() {
        String bodyId = driver.findElement(By.tagName("body")).getAttribute("id");
        Assert.assertEquals("order-confirmation", bodyId);
    }


}
