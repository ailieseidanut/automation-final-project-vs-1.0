Feature: A customer want to sign in

  Scenario: A customer want to sign in - positive scenario
    Given the user accesses to the main page
    And click on the Sign in button
    And fill the email address from Already registered
    And fill the user password "Abc123.."
    When click on the Sign in button from Already registered
    Then The user will be successfully logged in