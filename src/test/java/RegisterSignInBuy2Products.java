import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import page.HomePage;

import java.util.concurrent.TimeUnit;

public class RegisterSignInBuy2Products {

    HomePage homePage;
    WebDriver driver;

    @Given("the user accesses to the main page")
    public void mainPage() {

        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://automationpractice.com/index.php");
    }

    @And("click on the Sign in button")
    public void signIn() {
        homePage = new HomePage(driver);
        homePage.signInButton();
    }

    @And("fills email address 'ion127@gmail.com'")
    public void fillEmail() {
        homePage.fillUserMail();
    }

    @And("click on the Create An Account button")
    public void createAnAccount() {
        homePage.clickOnRegisterButton();
    }

    @And("click on Title Mr.")
    public void clickMr() {
        homePage.clickOnTitle();
    }

    @And("fill First name \"Ion\"")
    public void fillFirstName() {
        homePage.fillFirstName();
    }

    @And("fill Last name \"Popescu\"")
    public void fillLastName() {
        homePage.fillLastName();
    }

    @And("fill Password \"Abc123..\"")
    public void fillpassword() {
        homePage.password();
    }

    @And("select 15 from day of birth")
    public void selectDayOfBirth() {
        homePage.select15DayOfBirth();
    }

    @And("select July from mounth of birth")
    public void selectMonthOfBirth() {
        homePage.selectJulyMonthOfBirth();
    }

    @And("select 1980 from year of birth")
    public void select1980OfBirth() {
        homePage.select1980YearOfBirth();
    }

    @And("click on Sign up for our newsletter")
    public void clickOnSignUpForOurNews() {
        homePage.clickSelectSignUpForOurNewsletter();
    }

    @And("click on Receive special offers from our partners")
    public void clickOnReceiveOffersFor() {
        homePage.clickOnReceiveOffers();
    }

    @And("fill address First name \"Cristi\"")
    public void fillFirstNameInYourAddress() {
        homePage.fillFirstNameInYourAddress();
    }

    @And("fill address Last name \"Georgescu\"")
    public void fillLastNameInYourAddress() {
        homePage.fillLastNameInYourAddress();
    }

    @And("fill Company \"Ion's Company\"")
    public void fillCompanyName() {
        homePage.fillcompanyName();
    }

    @And("fill address \"Iasi, street Nationala 56\"")
    public void fillAddress() {
        homePage.fillAddress();
    }

    @And("fill address Line 2 \"Iasi, street Nicolina 123\"")
    public void fillAddress1() {
        homePage.fillAddress1();
    }

    @And("fill City \"Iasi\"")
    public void fillCyty() {
        homePage.fillCity();
    }

    @And("select \"Georgia\" from state drop-down")
    public void SelectState() {
        homePage.selectState();
    }

    @And("fill Zip-Postal Code \"65788\"")
    public void fillZipCode() {
        homePage.fillPostalCode();
    }

    @And("select \"United States\" from state drop-down")
    public void selectCountry() {
        homePage.selectCountry();
    }

    @And("fill Additional information \"hello world\"")
    public void additionalInformation() {
        homePage.additionalInformation();
    }

    @And("fill Home number \"0789653546\"")
    public void homePhone() {
        homePage.homePhone();
    }

    @And("fill Mobile number \"0789653123\"")
    public void mobilePhone() {
        homePage.mobilePhone();
    }

    @And("fill Assign an address alias for future referince \"Bucharest\"")
    public void assignAnAlisAddress() {
        homePage.assignAnAliasAddress();
    }

    @When("clicks on Register button")
    public void registerButton() {
        homePage.registerButton();
    }

    @Then("Your account will be successfully created")
    public void checkAccount() {
        homePage.checkAccountCreation();
    }


    ///Sign in //

    @And("fill the email address from Already registered")
    public void fillUserEmail() {
        homePage.fillEmail();
    }

    @And("fill the user password \"Abc123..\"")
    public void fillUserPassword() {
        homePage.userPassword();
    }

    @When("click on the Sign in button from Already registered")
    public void clickSignIn() {
        homePage.signInButtonFromAlreadyRegister();
    }

    @Then("The user will be successfully logged in")
    public void userLoggedIn() {
        homePage.checkAccountCreation();
    }

    //// shopFlow///

    @And("click on T-short button")
    public void clickOnTShortButton() {
        homePage.selectTShort();
    }

    @And("click on Add to cart button for t-short")
    public void clickOnAddToCartButton() {
        homePage.addTocCartTShort();
    }

    @And("click Continue shoping")
    public void clickOnContinueButton1() {
        homePage.continueShoping();
    }

    @And("click Dress button")
    public void clickOnDressButton() {
        homePage.clickOnDressButton();
    }

    @And("click Casual dress")
    public void clickOnCasualDress() {
        homePage.clickOnCassualDress();
    }

    @And("click on Add to cart button for dress")
    public void clickOnAddToCartDress() {
        homePage.addTocCartTShort2();
        homePage.clickAddToCart3();
    }

    @And("click Proceeded to checkout")
    public void clickOnProceededToCheckout() {
        homePage.proceededToCheckout();
    }

    @And("click on proceeded to checkout from summary")
    public void clickOnCheckOutSummary() {
        homePage.clickproceedToCheckoutButtonSummary();
    }

    @And("click on Proceeded to checkout from address")
    public void clickOnCheckOutAddress() {
        homePage.clickproceedToCheckoutButtonAddress();
    }

    @And("click on I agree with terms")
    public void clickOnIAgreeTerms() {
        homePage.agreewithTerms();
    }

    @And("click on Proceeded to checkout shipping")
    public void clickOnCheckOutShipping() {
        homePage.clickproceedToCheckoutButtonShipping();
    }

    @And("click on Pay by bank wire")
    public void clickOnPayBank() {
        homePage.clickPayByBankWire();
    }

    @When("click on i confirm my order")
    public void clickOnConfirmOrder() {
        homePage.clickConfirmOrder();
    }

    @Then("the order is confirmed")
    public void pageConfirmed() {
        homePage.confirmationPage();
    }


}



