Feature: Register new customer

  Scenario: Register new customer - positive scenario
    Given the user accesses to the main page
    And click on the Sign in button
    And fills email address 'ion127@gmail.com'
    And click on the Create An Account button
    And click on Title Mr.
    And fill First name "Ion"
    And fill Last name "Popescu"
    And fill Password "Abc123.."
    And select 15 from day of birth
    And select July from mounth of birth
    And select 1980 from year of birth
    And click on Sign up for our newsletter
    And click on Receive special offers from our partners
    And fill address First name "Cristi"
    And fill address Last name "Georgescu"
    And fill Company "Ion's Company"
    And fill address "Iasi, street Nationala 56"
    And fill address Line 2 "Iasi, street Nicolina 123"
    And fill City "Iasi"
    And select "Georgia" from state drop-down
    And fill Zip-Postal Code "65788"
    And select "United States" from state drop-down
    And fill Additional information "hello world"
    And fill Home number "0789653546"
    And fill Mobile number "0789653123"
    And fill Assign an address alias for future referince "Bucharest"
    When clicks on Register button
    Then Your account will be successfully created
