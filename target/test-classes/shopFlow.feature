Feature: A customer want to buy 2 products

  Scenario: A customer want to buy a Faded Short Sleeve T-shirts and a casual dress - positive scenario
    Given the user accesses to the main page
    And click on the Sign in button
    And fill the email address from Already registered
    And fill the user password "Abc123.."
    And click on the Sign in button from Already registered
    And The user will be successfully logged in
    And click on T-short button
    And click on Add to cart button for t-short
    And click Continue shoping
    And click Dress button
    And click Casual dress
    And click on Add to cart button for dress
    And click Proceeded to checkout
    And click on proceeded to checkout from summary
    And click on Proceeded to checkout from address
    And click on I agree with terms
    And click on Proceeded to checkout shipping
    And click on Pay by bank wire
    When click on i confirm my order
    Then the order is confirmed



